/**
 * @file main.c File con la procedura main. Contiene il flusso principale del programma, con la creazione dei segmenti di
 * memoria, la creazione dei processi figli e la distribuzione del lavoro.
 */
/*
 * main.c
 *
 *  Created on: 09 mag 2017
 *  Author: Bertoncelli Giovanni
 */
#include "main.h"

/// Flusso di programma principale
int main(int argc, char *argv[]) {

	///SIGINT binding di una funzione per il segnale di CTRL C
	signal(SIGINT, signal_ctrl_c);

	/// controllo dei parametri ricevuti @see check_parameters
	check_parameters(argc, argv);

	/// recupero dei parametri passati
	char *path_A = argv[1];
	char *path_B = argv[2];
	char *path_dest = argv[3];

	/// conversione numeri tramite atoi
	int order = atoi(argv[4]);
	int n_worker = atoi(argv[5]);

	long n_operations = order * order;

	_println("Verifica correttezza indici: ");

	/// verifica file @see check_index
	_printf("File matrice A...");

	check_index(path_A, order);

	_println("OK");

	_printf("File matrice B...");

	check_index(path_B, order);

	_println("OK");

	_println("Allocazione della memoria condivisa");
	_println("Allocazione memoria per la struttura di controllo");

	/// struttura globale di controllo (shmget) @see child_data, _ftok
	child_data_id = shmget(_ftok("main.c", ID_CHILD_CTL), sizeof(child_data),
			(IPC_CREAT | IPC_EXCL | 0777));

	if (child_data_id == -1) {
		critical_err("shmget: CHILD_DATA");
	}

	/// recupero la struttura dalla memoria condivisa
	child_data *data_pointer = (child_data *) shmat(child_data_id, NULL, 0);
	if (data_pointer == (child_data *) -1) {
		critical_err("shmmat: CHILD_DATA");
	}

	/// ihizializzazione struttura dati con dati di default
	data_pointer->A_id = -2;
	data_pointer->B_id = -2;
	data_pointer->C_id = -2;
	data_pointer->SUM_id = -2;
	data_pointer->pipes = (int *) -2;
	data_pointer->n_worker = n_worker;
	data_pointer->n_order = order;
	data_pointer->QUEUE_id = -2;
	data_pointer->SEM_id = -2;

	_println("Allocazione matrice A");

	/// creazione della memoria condivisa A (shmget)
	int mem_id_A = shmget(_ftok(".", ID_A), sizeof(signed int) * n_operations,
			(IPC_CREAT | IPC_EXCL | 0777));
	if (mem_id_A == -1) {
		critical_err("shmget: matrice A");
	}

	data_pointer->A_id = mem_id_A;

	/// lettura del file e allocazione della matrice in shm @see read_file
	read_file(path_A, mem_id_A);

	/// creazione della memoria condivisa B (shmget)
	_println("Allocazione matrice B");

	int mem_id_B = shmget(_ftok("..", ID_B), sizeof(signed int) * n_operations,
			(IPC_CREAT | IPC_EXCL | 0777));
	if (mem_id_B == -1) {
		critical_err("shmget: matrice B");
	}

	data_pointer->B_id = mem_id_B;

	read_file(path_B, mem_id_B);

	_println("Allocazione matrice C");

	/// creazione della memoria condivisa C (shmget)
	int mem_id_C = shmget(_ftok("/tmp", ID_C), (sizeof(signed int) * n_operations),
			(IPC_CREAT | IPC_EXCL | 0777));
	if (mem_id_C == -1) {
		critical_err("shmget: matrice C");
	}

	data_pointer->C_id = mem_id_C;

	_println("Allocazione memoria per la somma");


	/// creazione della memoria condivisa SUM (shmget)
	int mem_id_sum = shmget(_ftok("./doc", ID_SUM), sizeof(signed int),
			(IPC_CREAT | IPC_EXCL | 0777));
	if (mem_id_sum == -1) {
		critical_err("shmget: SUM");
	}

	data_pointer->SUM_id = mem_id_sum;

	// binding memoria per sum
	int *sum = shmat(mem_id_sum, NULL, 0);
	if (sum == (int *) -1) {
		critical_err("shmmat: SUM");
	}
	/// imposto a zero SUM
	*sum = 0;

	/// LOGICA PRINCIPALE =====================================================================================
	char string[200];
	sprintf(string, "Padre: %i", getpid());
	_println(string);

	/// j: iteratore provvisorio
	int j = 0;

	/// == PIPES ==
	/// strutture di controllo dei processi figli
	int pipes[n_worker * 2];

	/// allocazione delle pipe
	_println("Creazione delle pipes");


	for (j = 0; j < n_worker; j++) {
		int result = pipe(&(pipes[j * 2])); // ogni due
		if (result == -1)
			critical_err("pipe: a CHILD pipe");
	}

	/// Linking l'elenco delle pipe alla struttura generica di contro controllo
	data_pointer->pipes = pipes;

	/// == SEMAFORO ==
	/// Inizializzazione semafori per la verifica della terminazione dei processi
	_println("Creazione dei semafori");

	int sem_id = semget(ID_SEM, 2, IPC_CREAT | IPC_EXCL | 0666);
	/// semaforo[0] -> sincro padre e figli
	/// semaforo[1] -> accesso a somma
	if (sem_id == -1)
		critical_err("semget: parent");

	/// imposto il semaforo al numero di processi che devo attendere si creino
	if (semctl(sem_id, 0, SETVAL, n_worker) == -1)
		critical_err("semctl: inizializzazione");

	/// binding id semaforo
	data_pointer->SEM_id = sem_id;

	/// == CODA MESSAGGI ==
	/// creazione della coda di messaggi
	_println("Creazione coda messaggi");
	int msg_queue_id = msgget(_ftok("include", ID_MSG),
			IPC_CREAT | IPC_EXCL | 0666);
	if (msg_queue_id == -1)
		critical_err("msgget: coda messaggi");

	/// binding del descrittore della coda
	data_pointer->QUEUE_id = msg_queue_id;

	/// array con i pid di tutti i processi attivi
	pid_t active_processes_pid[n_worker];

	_println("Fork processi richiesti");

	/// == FORK ==
	/// alloco tutti i processi richiesti
	for (j = 0; j < n_worker; j++) {

		int current_pid;

		if ((current_pid = fork()) == 0) { /// SOLO PROCESSI FIGLI {

			my_pid_index = j;

			close(data_pointer->pipes[(j * 2) + 1]); /// chiusura canale scrittura per il padre

			signal(SIGTERM, child_termination); ///SIGTERM associo un segnale per la terminazione
			signal(SIGINT, SIG_IGN); ///SIGINT evito che CTRL+C ereditato dal padre termini i figli senza consenso
			signal(SIGUSR1, child_multiplication); ///SIGUSR1 associo un segnale al figlio per la moltiplicazione
			signal(SIGUSR2, child_sum); ///SIGUSR2 associo un segnale per la somma

			sprintf(string, "%i, Figlio: %i - padre: %i", j, getpid(),
					getppid());
			_println(string);

			/// decremento il semaforo = un processo è stato inizializzato
			/// un processo si è liberato! DECREMENTO IL SEMAFORO per eventualmente dare il via libera al main
			struct sembuf decrement_ctl;
			decrement_ctl.sem_flg = 0;
			decrement_ctl.sem_num = 0;
			decrement_ctl.sem_op = -1; /// decremento di 1
			if (semop(data_pointer->SEM_id, &decrement_ctl, 1) == -1)
				critical_err("Semop: CHILD - unlocking");

			while (true)
				pause(); /// FIGLI: li metto in attesa di un segnale
		}

		if (current_pid < 0)
			critical_err("fork");

		/// aggiungo il pid del nuovo processo figlio appena creato
		active_processes_pid[j] = current_pid;
	}

	struct sembuf wait_ctl;
	wait_ctl.sem_flg = 0;
	wait_ctl.sem_num = 0;
	wait_ctl.sem_op = 0; /// attendo si azzeri il semaforo per attendere la creazione di tutti i figli
	if (semop(sem_id, &wait_ctl, 1) == -1)
		critical_err("Semop: FATHER - waiting");

	for (j = 0; j < n_worker; j++)
		/// chiudo il canale di lettura per il padre
		close(pipes[(j * 2)]);

	/// MOLTIPLICAZIONE DELLA MATRICE ===============================
	int remaining_ops = n_operations;
	int row = 0, columns = 0;

	/// imposto il semaforo al numero di operazioni mancanti
	/// quando tutti i processi le avranno finite il main proseguirà
	if (semctl(sem_id, 0, SETVAL, remaining_ops) == -1)
		critical_err("semctl: inizializzazione");

	_println("Moltiplicazione A*B");

	j = 0;
	int index_pipes;

	while (remaining_ops > 0) { /// ci sono ancora operazioni da fare...

		index_pipes = j; /// intanto imposto l'indice al numero del pid nell'array
		/// se ricevo un messaggio sarà impostato all'indice dell'array del processo che ho trovato libero

		if (j >= n_worker) {

			/// IF finiti i processi leggo per quelli che si sono liberati
			/// variabile per memorizzare i messaggi
			message_frm messages_buffer;

			/// aspetto un messaggio dalla coda @see message_frm
			msgrcv(msg_queue_id, &messages_buffer, sizeof(message_frm),
			MSG_TYPE, 0);

			/// qualcuno si è liberato
			pid_t free_process_pid = messages_buffer.mpid;

			/// cerco l'indice della pipe a cui inviare il segnale
			index_pipes = index_from_pid(active_processes_pid, free_process_pid,
					n_worker);
		}

		char current_row_string[21];
		sprintf(current_row_string, "%d", row);

		char current_columns_string[21];
		sprintf(current_columns_string, "%d", columns);

		kill(active_processes_pid[index_pipes], SIGUSR1); /// invio al processo il segnale di moltiplicazione

		/// invio sulla pipe gli interi (in stringa) della riga e della colonna da inviare
		if (write(pipes[(index_pipes * 2) + 1], current_row_string,
				sizeof(current_row_string)) == -1)
			critical_err("Write: pipe to child");
		if (write(pipes[(index_pipes * 2) + 1], current_columns_string,
				sizeof(current_columns_string)) == -1)
			critical_err("Write: pipe to child");

		/// un operazione in meno da fare
		remaining_ops--; /// questa variabile mi garantisce sempre di non eccedere negli indici delle celle
		j++;
		columns++; /// prossima colonna

		if (columns == order) { /// IF finita la riga
			columns = 0;
			row++; /// incremento la riga
		}

	}

	/// attendo che tutti i processi terminino la MOLTIPLICAZIONE (SEM 0)
	wait_ctl.sem_flg = 0;
	wait_ctl.sem_num = 0;
	wait_ctl.sem_op = 0; /// attendo si azzeri
	if (semop(sem_id, &wait_ctl, 1) == -1)
		critical_err("Semop: FATHER - waiting");

	/// FLUSHING CODA MESSAGGI ======
	/// elimino la coda e la ricreo (per evitare conflitti) = svuoto la coda
	msgctl(msg_queue_id, IPC_RMID, NULL);
	/// creazione della coda di messaggi
	msg_queue_id = msgget(_ftok("include", ID_MSG),
			IPC_CREAT | IPC_EXCL | 0666);
	if (msg_queue_id == -1)
		critical_err("msgget: coda messaggi");

	/// binding del descrittore della coda
	data_pointer->QUEUE_id = msg_queue_id;

	/// SOMMA DELLA MATRICE ==========================
	remaining_ops = order; /// numero righe da sommare
	int current_row_sum = 0;
	j = 0;

	/// imposto il semaforo al numero di operazioni mancanti
	/// quando tutti i processi le avranno finite il main proseguirà
	if (semctl(sem_id, 0, SETVAL, remaining_ops) == -1)
		critical_err("semctl: inizializzazione");

	if (semctl(sem_id, 1, SETVAL, 1) == -1)
		critical_err("semctl: inizializzazione");

	_println("Somma Matrice C");

	while (remaining_ops > 0) {

		index_pipes = j; // intanto

		if (j >= n_worker) {

			/// variabile per memorizzare i messaggi
			message_frm messages_buffer;

			/// aspetto un messaggio dalla coda
			msgrcv(msg_queue_id, &messages_buffer, sizeof(message_frm),
			MSG_TYPE, 0);

			/// qualcuno si è liberato
			pid_t free_process_pid = messages_buffer.mpid;

			/// cerco l'indice della pipe a cui inviare il segnale
			index_pipes = index_from_pid(active_processes_pid, free_process_pid,
					n_worker);

		}

		char current_row_string[21];

		sprintf(current_row_string, "%d", current_row_sum);

		/// invio della riga su pipe
		if (write(pipes[(index_pipes * 2) + 1], current_row_string,
				sizeof(current_row_string)) == -1)
			critical_err("Write: pipe to child");

		kill(active_processes_pid[index_pipes], SIGUSR2); /// segnale per la somma della matrice C @see child_sum

		j++;
		remaining_ops--;
		current_row_sum++; /// prossima riga da sommare
	}

	/// attendo che tutti i processi terminino la SOMMA (SEM 0)
	wait_ctl.sem_flg = 0;
	wait_ctl.sem_num = 0;
	wait_ctl.sem_op = 0; /// attendo si azzeri
	if (semop(sem_id, &wait_ctl, 1) == -1)
		critical_err("Semop: FATHER - waiting");

	/// ==========================================================================================================

	_printf("Creazione file di output... ");

	/// creazione del file di output
	int output_descriptor = creat(path_dest, O_CREAT | 0777);

	if (output_descriptor < 0) {
		critical_err("creat: file output.");
	}

	/// scrittura della matrice C su file @see write_result_matrix
	write_result_matrix(output_descriptor, mem_id_C, order);

	close(output_descriptor);

	_println("OK");

	sprintf(string, "\nRisultato della somma di C = A*B: %d\n", *sum);
	_println(string);

	shmdt(sum);

	/// terminazione
	clean_exit(0);
}
