/**
 *	@file err_check_utils.c Contiene varie funzioni utili a gestire errori durante l'esecuzione del programma, errori nei parametri passati per riga di comando, ed errori nei file passati
 * in input (ordine scorretto o caratteri non ammessi) inoltre definisce funzioni per uscire correttamente dal processo senza lasciare allocata nessuna risorsa.
 * 	@brief Funzioni di gestione degli errori e controllo dei parametri (Processo PADRE principalmente).
 **/

#ifndef ERR_C
#define ERR_C

#include "../main.h"

///@name Funzioni di controllo dei parametri e file di input

/**
 * 	@brief La funzione check_parameters controlla che il numero di parametri passato sia corretto, che in caso i valori siano sempre > 0.
 * 	@brief In caso si può consultare un help di aiuto.
 *	@param argc: Numero di argomenti da linea di comando
 *	@param *argv[]: Array di puntatori alle stringhe passate come parametri (come nel main)
 *	@return bool: Esito funzione.
 */
bool check_parameters(char argc, char *argv[]) {

	/// numero parametri non corretto
	if (argc != 6) {
		if (argc > 1 && (strcmp("--help", argv[1]) == 0)) { /// richiesta --help
			char string[] =
					"Inserire: path matrici sorgente, path file destinazione, ordine delle matrici, numero di processi cooperanti\nsintassi: ./nome_programma [path matrice A] [path matrice B] [path matrice C] [ordine matrici] [n worker]\nAssicurarsi che il file di input contenga solamente spazi, new lines e numeri e finisca con una new line!\n";
			_printf(string);
		} else { /// gestione dell'errore
			char string[] =
					"Errore: numero parametri non corretto!\n--help per aiuto\n";
			write(2, string, sizeof(string));
		}
		clean_exit(1); /// exit code : 1
	}

	// parametri non corretti!
	/// strtol( stringa, array char, base ) converte una stringa in numero e ritorna 0 in caso di problemi
	if (strtol(argv[4], NULL, 10) == 0 || strtol(argv[5], NULL, 10) == 0) {
		char string[] = "Errore parametri numerici non corretti!\n";
		write(2, string, sizeof(string));
		clean_exit(1); /// exit code : 1
	}

	return true;
}

/**
 *	@brief La funzione check_index, seppur un po' ridondante rispetto alle funzioni di I/O su file, è essenziale per evitare Segmentation Fault e errori simili: infatti
 *	controlla che i file in input passati siano dell'ordine indicato da linea di comando, prima che venga allocata la memoria necessaria per contenere il tutto.
 *	Per fare questo usa degli indici che si aggiornano alla ricorrenza di " " o "\n" e vengono confrontati con l'ordine passato (matrici quadrate!).
 *	@param *path: File da controllare.
 *	@param i: Ordine richiesto della matrice.
 */
void check_index(char *path, int i) {

	/// apertura del file in sola lettura
	int file_descriptor = open(path, O_RDONLY, 0444);

	/// gestione dell'errore
	if (file_descriptor == -1) {
		critical_err("open");
	}

	char temp_buffer;
	int col_counter = 0;
	int row_counter = 0;

	/// lettura di un char alla volta
	while (read(file_descriptor, &temp_buffer, sizeof(char)) > 0) {

		if (temp_buffer == ' ') { /// IF finito un numero:
			col_counter++; /// colonna finita
		} else if (temp_buffer == '\n') { /// ELSE IF finita la riga
			row_counter++;
			col_counter++; // un altro ultimo numero...

			/// controlli sulla correttezza matrice
			if (col_counter != i) {

				char error[] =
						"(Almeno) una riga è scorretta! (Ordine scorretto o matrice scorretta!)\n";
				write(2, error, sizeof(error));

				clean_exit(1);
			}
			// azzero
			col_counter = 0;

		} else if (((temp_buffer > 31 && temp_buffer < 48)
				|| (temp_buffer > 57)) && temp_buffer != '-') { /// controllo caratteri non riconosciuti.
			char error_string[200];
			sprintf(error_string,
					"\nCaratteri non riconosciuti presenti (carattere: %c) \n",
					temp_buffer);
			critical_err(error_string);
		}

	}

	if (temp_buffer != '\n') { /// ultima eventuale riga
		row_counter++;
		if (++col_counter != i && temp_buffer != ' ') {
			char error[] =
					"(Almeno) una riga è scorretta! (Ordine scorretto o matrice scorretta!)\n";
			write(2, error, sizeof(error));

			clean_exit(1);
		}
	}

	if (row_counter != i) { /// controllo sul numero delle righe

		write(2, path, sizeof(path));
		write(2, ": ", sizeof(": "));

		char error[] =
				"Numero di righe scorretto! (Ordine scorretto o matrice scorretta!)\n";
		write(2, error, sizeof(error));
		clean_exit(1);
	}

	// chiusura del file
	close(file_descriptor);

}
/// @}

///@name Funzioni per la gestione facilitata degli errori e l'uscita corretta del programma

/**
 *	@brief La funzione critical_err viene invocata in caso di un errore grave che impedirebbe la prosecuzione del programma.
 *	 stampa a video un messaggio di errore e esce.
 *	@param string: String della funzione che ha generato l'errore
 */
void critical_err(char *string) {
	char error[181];

	sprintf(error, "\nErrore nella funzione: %s\n", string);
	write(2, error, strlen(error) + 1);

	clean_exit(1);
}

/**
 * 	@brief La funzione clean_exit permette di chiudere il programma, a causa di errore o per terminazione naturale, rimuovendo tutte le risorse e le IPCs del programma. Se essa è invocata per un errore non è possibile capire a che punto era il programma per questo prima di rimuovere una risorsa si controlla che non sia al suo valore di default (-2, arbitrario) e quindi non ancora allocata per evitare Segmentation Faults. La funzione sfrutta la struttura condivisa child_data per valutare le risorse già allocate e procedere alla rimozione.
 *	@param exit_code: Intero per la terminazione del programma con un determinato codice di terminazione (funzione exit() di libreria).
 */
void clean_exit(int exit_code) {

	///ATTENZIONE: In questa funzione ignoro gli errori delle System Call per evitare effetti a cascata.

	/// recupero la struttura dalla memoria condivisa (shmat)
	child_data *data_pointer = (child_data *) shmat(child_data_id, NULL, 0);

	// il programma non è nemmeno cominciato
	if (data_pointer == (child_data *) -1)
		exit(0);

	_println("Rimozione delle risorse allocate");

	/// eliminazione di tutte le risorse allocate nel sistema (tramite shmctl(IPC_RMID) oppure tramite semctl(IPC_RMID) o semctl(IPC_RMID)
	if (data_pointer->A_id != -2) /// controllo che non siano i valori di default
								  /// (per evitare segm fault)
		shmctl(data_pointer->A_id, IPC_RMID, NULL);

	if (data_pointer->B_id != -2)
		shmctl(data_pointer->B_id, IPC_RMID, NULL);

	if (data_pointer->C_id != -2)
		shmctl(data_pointer->C_id, IPC_RMID, NULL);

	if (data_pointer->SUM_id != -2)
		shmctl(data_pointer->SUM_id, IPC_RMID, NULL);

	if (data_pointer->QUEUE_id != -2)
		msgctl(data_pointer->QUEUE_id, IPC_RMID, NULL);

	if (data_pointer->SEM_id != -2) {
		semctl(data_pointer->SEM_id, 0, IPC_RMID);
		semctl(data_pointer->SEM_id, 1, IPC_RMID);
	}

	shmctl(child_data_id, IPC_RMID, NULL); /// rimozione struttura dati condivisa

	_println("Terminazione processi figli");

	kill(0, SIGTERM); /// terminazione dei processi figli tramite kill(0, SIGTERM)

	exit(exit_code); /// exit di libreria
}
/// @}

#endif
