/**
 *	@file gp_utils.c contiene alcune funzioni general-purpose, utili per il progetto.
*	@brief Funzioni general purpose
 **/

/*
 * gp_utils.c
 *
 *  Created on: 13 mag 2017
 *  Author: Bertoncelli Giovanni
 */
#ifndef GP_C
#define GP_C

#include "../main.h"

/**
 * @brief La funzione index_from_pid serve per recuperare l'indice della pipe corrispondente al pid dato. Nel main si ha un array di pipe grande due volte il numero dei processi (per RW), è utile recuperare l'indice corrispondente ad un PID per ottenere la sua pipe
 * @param *array_pids: Puntatore all'array dei pid
 * @param pid: Pid dato da cercare
 * @param n: Numero di processi, quindi grandezza di array_pids
 * @return int: L'indice trovato nell'array. In caso -1 (errore irreversibile).
 **/
int index_from_pid(pid_t *array_pids, int pid, int n) {

	int i; /// indice di scorrimento

	/// scorro l'array dei pid
	for (i = 0; i < n; i++) {
		// cerco il pid
		if (array_pids[i] == pid) { /// se il pid è uguale torno l'indice
			return i;
		}
	}

	return -1; /// errore
}
/**
 * @brief La funzione _ftok è una funzione che riprende la ftok() di libreria per gestire allo stesso tempo gli errori e poterla chiamare insieme ad un'altra funzione.
 * @param *path: Path del file che la ftok() converte in ID univoco
 * @param raw_id: Offset necessario alla ftok
 * @return key_t: Chiave univoca al sistema per la risorsa da allocare
 **/
key_t _ftok(const char *path, int raw_id) {
	key_t temp = ftok(path, raw_id);
	if (temp == -1) { // Error handling
		critical_err("ftok");
	}
	return temp;
}

/**
 * @brief La funzione valid_chars conta i caratteri validi di una stringa
 * @param *buffer: Stringa da leggere
 * @return int: # caratteri validi
 **/
int valid_chars(char *buffer) {
	int counter = 0;
	while (*buffer++ != '\0') { /// Conto finchè non trovo \0
		counter++;
	}
	// considero l'ultimo carattere
	return counter;
}

#endif
