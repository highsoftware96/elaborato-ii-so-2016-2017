/**
 *	@file children_functions.c Contiene tutte le routine che andranno eseguite dai processi figli una volta inviato loro il segnale associato alla funzione data.
 *	@brief Funzioni di moltiplicazione e di somma (per processi FIGLI).
 */
/*
 * children_functions.c
 *
 *  Created on: 12 mag 2017
 *  Author: Bertoncelli Giovanni
 */
#ifndef CHILD_C
#define CHILD_C

#include "../main.h"

/// @name Operazioni sulle matrici

/**
 * @brief La funzione child_multiplication è invocata dai processi figli al segnale SIGUSR1. Essendo invocata da un segnale non è possibile passare parametri, tuttavia si possono considerare parametri:
 * child_data_id (variabile globale) da cui il figlio recupererà tutte le risorse necessarie al calcolo e alla sincronizzazione tramite shmat, e da essa quindi l'ID della matrice A (shm), quello della B, quello della C, l'ID
 * della coda di messaggi, del semaforo da decrementare, l'array delle pipe e l'indice (globale) del processo corripondente.
 * Il figlio riceve riga e colonna da pipe e calcola il valore della cella (riga x colonna) corrispondente di C.
 */
void child_multiplication(int sig_num) {

	/// recupero la struttura dalla memoria condivisa
	child_data *data_pointer_ptr = (child_data *) shmat(child_data_id, NULL,
			IPC_CREAT | 0666);
	if (data_pointer_ptr == (child_data *) -1) {
		critical_err("shmmat: CHILD_DATA");
	};

	/// recupero ordine matrici
	int i = data_pointer_ptr->n_order;

	/// Recupero la memoria condivisa matrice A, B, C (CASTING a puntatore a matrice ixi)
	int (*matrix_A)[i][i] = (int (*)[i][i]) shmat(data_pointer_ptr->A_id, NULL,
			0);
	if ((matrix_A == (int (*)[i][i]) -1))
		critical_err("Shmat: child, matrix A");

	int (*matrix_B)[i][i] = (int (*)[i][i]) shmat(data_pointer_ptr->B_id, NULL,
			0);
	if (matrix_B == (int (*)[i][i]) -1)
		critical_err("Shmat: child, matrix B");

	int (*matrix_C)[i][i] = (int (*)[i][i]) shmat(data_pointer_ptr->C_id, NULL,
			0);
	if ((matrix_C == (int (*)[i][i]) -1))
		critical_err("Shmat: child, matrix C");

	/// buffer per la stringa/numero in arrivo (riga)
	char buffer_row[21];

	/// leggo i parametri dalla pipe
	if (read(data_pointer_ptr->pipes[my_pid_index * 2], buffer_row,
			sizeof(buffer_row)) == -1) /// my_pid_index * 2 permette di accedere al canale di lettura
		critical_err("Read: from pipe (1)");

	int row = strtol(buffer_row, NULL, 10); /// conversione in numero

	char buffer_col[21]; /// uguale per la colonna: lettura da pipe e conversione a intero
	if (read(data_pointer_ptr->pipes[my_pid_index * 2], buffer_col,
			sizeof(buffer_col)) == -1)
		critical_err("Read: from pipe (2)");

	int column = strtol(buffer_col, NULL, 10);

	/// PRODOTTO RIGHE COLONNE
	/// azzero il valore della cella nella shm di C
	(*matrix_C)[row][column] = 0;

	int j;

	for (j = 0; j < data_pointer_ptr->n_order; j++) { /// scorro riga row-esima di A e colonna column-esima di B e sommo i vari risultati
		(*matrix_C)[row][column] += (((*matrix_A)[row][j])
				* ((*matrix_B)[j][column]));
	}

	message_frm send_my_pid; /// creo un messaggio
	send_my_pid.mtype = MSG_TYPE;
	send_my_pid.mpid = getpid(); // ci salvo il mio pid

	if (msgsnd(data_pointer_ptr->QUEUE_id, &send_my_pid,
			sizeof(send_my_pid.mpid), 0) == -1) /// invio il messaggio sulla coda di messaggi ( non serve la get: l'ho ereditata dal padre)
		critical_err("Msgsnd: CHILD"); // lo invio

	/// rimozione links
	shmdt(matrix_A);
	shmdt(matrix_B);
	shmdt(matrix_C);

	/// un processo si è liberato! DECREMENTO IL SEMAFORO per eventualmente dare il via libera al main
	struct sembuf decrement_ctl; /// struttura di decremento del semaforo
	decrement_ctl.sem_flg = 0;
	decrement_ctl.sem_num = 0; /// semaforo n.0
	decrement_ctl.sem_op = -1; // "incremento" di -1
	if (semop(data_pointer_ptr->SEM_id, &decrement_ctl, 1) == -1) /// faccio l'operazione (atomica) sul semaforo
		critical_err("Semop: CHILD - unlocking");

	shmdt(data_pointer_ptr); /// unlinking della struttura child_data
}

/**
 * @brief La funzione child_sum è invocata dai processi figli al segnale SIGUSR2. Essendo invocata da un segnale non è possibile passare parametri, tuttavia si possono considerare parametri:
 * child_data_id (variabile globale) da cui il figlio recupererà tutte le risorse necessarie al calcolo e alla sincronizzazione tramite shmat, e da essa quindi l'ID della matrice C, l'ID
 * della coda di messaggi, del semaforo da decrementare, l'array delle pipe e l'indice (globale) del processo corripondente (in modo simile alla moltiplicazione) e ovviamente l'ID della memoria dove poi salvare
 * il risultato della somma.
 * Il figlio riceve la riga da pipe e calcola il valore della somma di quella riga della matrice C.
 */
void child_sum(int sig_num) {

	/// recupero child_data dati globali
	child_data *data_pointer_ptr = (child_data *) shmat(child_data_id, NULL, 0);

	if ((data_pointer_ptr == (child_data *) -1))
		critical_err("Shmat: child, CHILD DATA");

	int i = data_pointer_ptr->n_order;

	/// recupero la memoria condivisa matrice c, sum
	int (*matrix_C)[i][i] = (int (*)[i][i]) shmat(data_pointer_ptr->C_id, NULL,
			0);
	if ((matrix_C == (int (*)[i][i]) -1))
		critical_err("shmat: child, matrix C");

	/// recupero puntatore alla somma (shmat)
	int *sum = (int *) shmat(data_pointer_ptr->SUM_id, NULL, 0);
	if ((sum == (int *) -1))
		critical_err("shmat: child, SUM");

	int partial_result = 0, j;

	char buffer_row[21];

	/// lettura della riga da pipe
	if (read(data_pointer_ptr->pipes[my_pid_index * 2], buffer_row,
			sizeof(buffer_row)) == -1)
		critical_err("Read: from pipe (SUM)");

	int row = strtol(buffer_row, NULL, 10); /// conversione a numero

	/// CALCOLO SOMMA RIGA
	for (j = 0; j < data_pointer_ptr->n_order; j++) { /// scorro la riga e sommo
		partial_result += (*matrix_C)[row][j];
	}

	/// ACCESSO al SEMAFORO mutex ##################
	struct sembuf mutex_ctl;
	mutex_ctl.sem_flg = 0;
	mutex_ctl.sem_num = 1; /// semaforo n.1 di accesso a *sum
	mutex_ctl.sem_op = -1; /// PROVO A DECREMENTARE

	if (semop(data_pointer_ptr->SEM_id, &mutex_ctl, 1) == -1) /// Eseguo: attesa che il semaforo passi a un valore positivo
		critical_err("semop: child mutex");

	/// SEZIONE CRITICA !!
	*sum += partial_result;

	mutex_ctl.sem_flg = 0;
	mutex_ctl.sem_num = 1; /// semaforo n.1 di accesso a *sum
	mutex_ctl.sem_op = 1; /// PROVO A DECREMENTARE

	/// SEMAFORO USCITA ###################
	if (semop(data_pointer_ptr->SEM_id, &mutex_ctl, 1) == -1) /// Eseguo: incremento il semaforo
		critical_err("semop: child mutex");

	///invio messaggio PID
	message_frm send_my_pid; // creo un messaggio
	send_my_pid.mtype = MSG_TYPE;
	send_my_pid.mpid = getpid(); // ci salvo il mio pid

	if (msgsnd(data_pointer_ptr->QUEUE_id, &send_my_pid,
			sizeof(send_my_pid.mpid), 0) == -1)
		critical_err("Msgsnd: CHILD"); // lo invio

	/// rimozione dei links
	shmdt(matrix_C);
	shmdt(sum);

	/// SEMAFORO per main: un operazione in meno da fare
	struct sembuf decrement_ctl;
	decrement_ctl.sem_flg = 0;
	decrement_ctl.sem_num = 0;
	decrement_ctl.sem_op = -1; /// decremento di 1
	if (semop(data_pointer_ptr->SEM_id, &decrement_ctl, 1) == -1)
		critical_err("Semop: CHILD - unlocking");

	shmdt(data_pointer_ptr);
}
/// @}

/**
 * @brief Funzione di terminazione di un processo figlio: chiama semplicemente exit(0). Generata dal segnale SIGTERM.
 */
void child_termination() {
	exit(0);
}

#endif
