/** 
 @file signal_handlers.c: file con funzioni per la gestione dei segnali per il main
 @brief Funzioni signal handlers essenziali
 **/
/*
 * signal_handlers.c
 *
 *  Created on: 12 mag 2017
 *  Author: Bertoncelli Giovanni
 */

#ifndef SIGN_C
#define SIGN_C

#include "../main.h"

/** 
 @brief La funzione signal_ctrl_c è la procedura che il main mette in atto quando da terminale si esegue la scorciatoia di terminazione CTRL-C. Prevede che si mostri una richiesta di conferma prima di terminare tutto il processo padre. Se si conferma tramite la clean_exit si chiude il programma correttamente e rimovendo le risorse impegnate.
 @return void
 **/
void signal_ctrl_c(int sig_num) {

	char string[] = "\nSicuri di voler terminare il programma? (s/n) ";
	_printf(string);
	char response;
	read(0, &response, sizeof(char));
	if (response == 's' || response == 'S')
		clean_exit(1);

}

#endif
