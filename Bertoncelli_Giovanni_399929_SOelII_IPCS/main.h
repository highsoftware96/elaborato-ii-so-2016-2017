/*
 * main.h
 *
 *  Created on: 09 mag 2017
 *      Author: gberto96
 */
/**
 * @mainpage Descrizione dello svolgimento
 * @brief Il programma prevede principalmente questo flusso che:
 * - Il main controlla parametri e file, alloca risorse quali memoria condivisa (per matrice A, B, C), pipes (2 * n_worker) e semafori (uno per la sincronizzazione tra figli e padre e uno per la mutex tra figli per l'accesso alla somma in shm)
 * e una coda di messaggi per la comunicazione tra figli e padre.
 * - Dopo aver definito un numero di operazioni da fare il padre, finchè ci sono operazioni da fare invia sulla pipe le informazioni e fa la kill dei processi che devono lavorare su esse. Se il numero dei processi allocati è uguale a 0 il padre deve attendere che qualcuno si liberi e per far questo quando un certo contatore j supera il numero di processi il padre attende che qualche processo si liberi aspettando che arrivino messaggi sulla coda. I messaggi ricevuti conterranno il pid del processo libero, quindi il padre procede in modo analogo a prima, dopo aver cercato l'indice delle pipe di quel processo.
 * - Quando tutti hanno terminato il semaforo intero inizializzato al numero di operazioni da fare sblocca il padre che procede a distribuire le operazioni di somma
 * - Dopo la moltiplicazione il padre procede a svuotare la coda di messaggi per renderla utile alle operazioni (distinte) della somma
 * - La somma procede in modo perfettamente analogo a quanto sopra: un ciclo sui processi e uno sulle operazioni rimanenti attendendo i pid sulla coda.\n
 *
 *
 * Tutte le operazioni vengono effettuate dai figli tramite gli appositi segnali che ricevono (SIGUSR1 moltiplicazione, SIGUSR2 somma, SIGTERM terminazione), di conseguenza
 * i figli una volta inizializzati sono in costante pausa.\n\n
 * \n
 * ANNOTAZIONI IMPORTANTI:\n
 * - FORMATO DEI FILE: il formato dei file di input delle matrici prevede la separazione delle colonne tramite SINGOLO SPAZIO, mentre la separazione delle righe tramite NEW LINE; inoltre l'ultimo numero
 * deve essere seguito o da NEW LINE o da nessun altro carattere. Caratteri o simboli sconosciuti verranno segnalati con conseguente chiusura del programma. Sono ammessi numeri negativi.
 *\n
 * - Nella cartella data\ test/ sono stati inseriti degli esempi di file di matrici per verificare il corretto funzionamento del programma (2 matrici 4x4, 2 5x5 e 2 7x7)
 * \n
 * - Nella cartella doc/html/ è inserita la documentazione in HTML generata da Doxygen, gli altri file presenti nella cartella doc/ sono di supporto alla creazione della documentazione e vanno mantenuti. Nella cartella include/ invece tutti i file C con le funzioni di supporto.
 * \n
 * - Inoltre è stato fatto un MakeFile per la compilazione e la generazione della documentazione.
\n\n 
@image html 1.png
 \n\n
 @image html 2.png
 \n\n
 */

/**
 * @file main.h File header contenente tutte le strutture globali, le variabili per il preprocessore e le anteprima delle funzioni.
 */
#ifndef MAIN_H_
#define MAIN_H_

#include <sys/types.h>
#include <stdbool.h>

/// uniche variabili globale -> serve per recuperare in
/// shm tutte le risorse necessarie ai figli
/// tengo poi traccia dell'indice universale id
int child_data_id = -2;
/// indice necessario ai figli per identificare la propria pipe
int my_pid_index;

/// Variabili globali: risorse comuni

/// La struttura child_data è definita per l'uso comune di risorse ricorrenti sia per il padre che per i figli.
/// Verrà allocata in memoria condivisa in modo che tutti possano avere accesso ad essa. Ovvio che il descrittore della shm sarà globale.
typedef struct {
	/// Descrittore shm di A
	int A_id;
	/// Descrittore shm di B
	int B_id;
	/// Descrittore shm di C
	int C_id;
	/// Descrittore shm della somma
	int SUM_id;
	/// Puntatore all'array dei descrittori delle pipes
	int *pipes;
	/// n_order ordine matrice, n elementi array di controllo
	int n_order;
	/// n_order processi
	int n_worker;
	/// Descrittore della coda dei messaggi
	int QUEUE_id;
	/// Descrittore semafori
	int SEM_id;
} child_data;

/// Struttura globale per il format dei messaggi da inviare e ricevere
typedef struct {
	/// Tipo messaggio (MSG_TYPE) @see MSG_TYPE, children_functions.c
	long mtype;
	/// Testo messaggio: pid del processo
	pid_t mpid;
} message_frm;

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <sys/shm.h>
#include <sys/ipc.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <signal.h>
#include <sys/msg.h>
#include <sys/sem.h>

/// offset area A
#define ID_A 76
/// offset area B
#define ID_B 88
/// offset area C
#define ID_C 99
/// offset area SUM
#define ID_SUM 111
/// offset area controllo
#define ID_CHILD_CTL 118
/// offset coda messaggi
#define ID_MSG 432
/// id messaggi
#define MSG_TYPE 1
/// offset semaforo
#define ID_SEM 1542

void critical_err(char *string);
void clean_exit(int exit_code);
bool check_parameters(char argc, char *argv[]);
void read_file(char *path, int mem_id_A);
void check_index(char *path, int i);
key_t _ftok(const char *path, int raw_id);
void signal_ctrl_c(int sig_num);
int index_from_pid(pid_t *array_pids, int pid, int n);
void write_result_matrix(int file_descriptor, int matrix_C_id, int ordine);
int valid_chars(char *buffer);
void _println(char *message);
void _printf(char *message);

/// Inclusione dei file con le funzioni essenziali
#include "include/signal_handlers.c"
#include "include/io_utils.c"
#include "include/err_check_utils.c"
#include "include/children_functions.c"
#include "include/gp_utils.c"

#endif /* MAIN_H_ */
