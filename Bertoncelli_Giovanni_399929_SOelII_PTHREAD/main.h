/*
 * main.h
 *
 *  Created on: 09 mag 2017
 *      Author: gberto96
 */
/**
 * @mainpage Descrizione dello svolgimento (ELABORATO facoltativo con Thread Posix)
 * @brief Il programma prevede principalmente questo flusso che:
 * - Il main controlla parametri e file, alloca risorse quali memoria condivisa (per matrice A, B, C), un intero per la dimensione delle matrici, un puntatore globale per la memorizzazione dei TID dei thread.
 * - Si procede nelle due fasi di Somma e moltiplicazione. Per entrambe si inviano tramite una struttura apposita tutti i dati per l'elaborazione al momento della creazione della thread stessa. Quindi dopo ogni fase il main aspetta le thread tramite un ciclo for di join sulle thread create
 * - Per ultimo si procede a stampare su file la matrice C, a stampare a video la somma, ad eliminare le risorse allocate sullo heap, quindi terminare il programma.
 * \n\n\n
 * @brief ANNOTAZIONI IMPORTANTI:\n
 * - FORMATO DEI FILE: il formato dei file di input delle matrici prevede la separazione delle colonne tramite SINGOLO SPAZIO, mentre la separazione delle righe tramite NEW LINE; inoltre l'ultimo numero
 * deve essere seguito o da NEW LINE o da nessun altro carattere. Caratteri o simboli sconosciuti verranno segnalati con conseguente chiusura del programma.
 *\n
 * - Nella cartella data\ test/ sono stati inseriti degli esempi di file di matrici per verificare il corretto funzionamento del programma (2 matrici 4x4, 2 5x5 e 2 7x7)
 * \n
 * - Nella cartella doc/html/ è inserita la documentazione in HTML generata da Doxygen, gli altri file presenti nella cartella doc/ sono di supporto alla creazione della documentazione e vanno mantenuti. Nella cartella include/ invece tutti i file C con le funzioni di supporto.
 * \n
 * - Inoltre è stato fatto un MakeFile per la compilazione e la generazione della documentazione.
 */

/**
 * @file main.h File header contenente tutte le strutture globali, le variabili per il preprocessore e le anteprima delle funzioni.
 */
#ifndef MAIN_H_
#define MAIN_H_

#include <sys/types.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>

/// struct passata nella moltiplicazione alle thread
typedef struct {
	/// id della thread
	int id;
	/// indice della riga
	int Arow;
	 /// indice della colonna
	int Bcol;
} package_t;

/// struct passata nella somma alle thread
typedef struct {
	/// puntatore al semaforo per la sezione critica
	pthread_mutex_t *mutex;
	/// Riga da sommare
	int Crow;
} package_t_sum;

void critical_err(char *string);
void clean_exit(int exit_code);
bool check_parameters(char argc, char *argv[]);
void read_file(char *path, int **matrix);
void check_index(char *path, int i);
void write_result_matrix(int file_descriptor, int **matrix_C);
int valid_chars(char *buffer);
void _println(char *message);
void _printf(char *message);
void multiply(int row, int column);
void *thread_multiply_routine(void *arg);
void sum(int row, pthread_mutex_t *mutex_sum);

/// Inclusione dei file con le funzioni essenziali
#include "include/io_utils.c"
#include "include/err_check_utils.c"
#include "include/gp_utils.c"
#include "include/thread_utils.c"

#endif /* MAIN_H_ */
