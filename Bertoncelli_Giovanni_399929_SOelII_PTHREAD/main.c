/**
 *@file main.c File principale col flusso del main()
 */
#include <pthread.h>

/// Grandezza globale delle matrici
int size_global;
/// puntatore globale all'array dei TID
pthread_t *threads = (pthread_t *) -2;
/// Puntatore alla matrice A
signed int **MA = (int **) -2;
/// Puntatore alla matrice B
signed int **MB = (int **) -2;
/// Puntatore alla matrice C
signed int **MC = (int **) -2;
/// Puntatore alla somma
signed int *SUM = (int *) -2;

#include "main.h"

/**
 Il main alloca le matrici, assegna i loro valori
 e crea le thread per eseguire la moltiplicazione
 riga * colonna e successivamente fa la stessa cosa con la somma
 */
int main(int argc, char *argv[]) {

	/// controllo dei parametri ricevuti @see check_parameters
	check_parameters(argc, argv);

	/// recupero dei parametri passati
	char *path_A = argv[1];
	char *path_B = argv[2];
	char *path_dest = argv[3];

	/// conversione numeri tramite atoi
	size_global = atoi(argv[4]);

	_println("Verifica correttezza indici");

	/// verifica file @see check_index
	_printf("File matrice A...");

	check_index(path_A, size_global);

	_println("OK");

	_printf("File matrice B...");

	check_index(path_B, size_global);

	_println("OK");

	int row = 0, column = 0, i = 0;

	/// alloco tutte le matrici sullo Heap tramite _malloc @see _malloc
	/// sono puntatori di puntatori in modo che per ogni puntatore creo una riga della matrice
	_printf("Allocazione memoria matrice A...");
	MA = (int **) _malloc(size_global * sizeof(int *));
	for (i = 0; i < size_global; i++)
		MA[i] = (int *) _malloc(size_global * sizeof(int));
	_println("OK");

	_printf("Allocazione memoria matrice B...");
	MB = (int **) _malloc(size_global * sizeof(int *));
	for (i = 0; i < size_global; i++)
		MB[i] = (int *) _malloc(size_global * sizeof(int));
	_println("OK");

	_printf("Allocazione memoria matrice C...");
	MC = (int **) _malloc(size_global * sizeof(int *));
	for (i = 0; i < size_global; i++)
		MC[i] = (int *) _malloc(size_global * sizeof(int));
	_println("OK");

	/// INIZIALIZZAZIONE MATRICI A e B
	/// leggo da file e salvo sulle matrici globali
	_println("Lettura dei file delle matrici...");

	_printf("Matrice A...");
	read_file(path_A, MA);
	_println(" OK");

	_printf("Matrice B...");
	read_file(path_B, MB);
	_println(" OK");

	/// INIZIALIZZAZIONE MATRICE C
	/// imposto la matrice C tutta a 0
	for (row = 0; row < size_global; row++) {
		for (column = 0; column < size_global; column++) {
			MC[row][column] = 0;
		}
	}

	/// PRODOTTO
	/// Impostazioni thread ==

	/// spazio in memoria per i TID delle thread @see _malloc
	_println("Allocazione memoria per TID");
	threads = (pthread_t *) malloc(
			(sizeof(pthread_t) * (size_global * size_global)));/* per tenere traccia degli identificatori delle thread */

	package_t *thread_data; /** argomento per le thread */
	pthread_attr_t attr; /** attibuto per le thread*/

	/// ATTRIBUTO JOINABLE per le thread
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

	/// ==

	/// MOLTIPLICAZIONE ==
	row = 0;
	column = 0, i = 0;

	/// Per ogni cella:
	for (i = 0; i < (size_global * size_global); i++) {

		/// allocazione dei dati per la thread
		thread_data = (package_t *) _malloc(sizeof(package_t));
		thread_data->id = i;
		thread_data->Arow = row;
		thread_data->Bcol = column;

		/// creazione della thread con argomenti annessi
		if (pthread_create((threads + i), &attr, thread_multiply_routine,
				thread_data) == -1) {
			critical_err("pthread_create");
		}

		/// offset vari delle matrici
		column++;

		if (column == size_global) {
			column = 0;
			row++;
		}
	}
	/// ==

	/// Attesa delle thread
	for (i = 0; i < size_global * size_global; i++) {
		if (pthread_join(*(threads + i), NULL) == -1) {
			critical_err("Join");
		}
	}

	/// Liberazione spazio TID vecchi e attr
	free(threads);
	pthread_attr_destroy(&attr);

	/** Stampa dei risultati */
	_println("Stampa della matrice C su file");

	/// creazione file (eventuale)
	int output_descriptor = creat(path_dest, O_CREAT | 0777);

	if (output_descriptor < 0) {
		critical_err("creat: file output.");
	}

	/// scrittura della matrice C su file @see write_result_matrix
	write_result_matrix(output_descriptor, MC);

	close(output_descriptor);

	_println("SOMMA\nAllocazione memoria per TID");

	/// SOMMA ==
	/// Impostazioni thread
	/// spazio in memoria per i TID delle thread
	threads = (pthread_t *) _malloc((sizeof(pthread_t) * size_global));/* per tenere traccia degli identificatori delle thread */

	/// ATTRIBUTO JOINABLE per le thread
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

	/// struttura da passare alla thread
	package_t_sum *thread_data_sum;

	/// Malloc dello spazio per la somma
	SUM = (int *) _malloc(sizeof(int));
	*SUM = 0;

	///Inizializzo il semaforo
	_println("Inizializzazione semaforo");
	pthread_mutex_t mutex_sum;

	pthread_mutexattr_t mutex_attributes;

	// inizializzazione semaforo
	if (pthread_mutex_init(&mutex_sum, &mutex_attributes) == -1) {
		critical_err("Errore init mutex");
	}

	/// ciclo for sulle righe di C
	for (i = 0; i < size_global; i++) {

		/// allocazione dei dati per la thread
		thread_data_sum = (package_t_sum *) _malloc(sizeof(package_t_sum));
		thread_data_sum->Crow = i;
		/// Inserisco il semaforo nei dati da inviare alla thread
		thread_data_sum->mutex = &mutex_sum;

		/// creazione della thread con argomenti annessi
		if (pthread_create((threads + i), &attr, thread_sum_routine,
				thread_data_sum) == -1)
			critical_err("pthread_create");

	}

	/// Joining delle thread
	for (i = 0; i < size_global; i++) {
		if (pthread_join(*(threads + i), NULL) == -1)
			critical_err("Join");
	}

	/// Distruzione semaforo e attributi
	pthread_mutex_destroy(&mutex_sum);
	pthread_attr_destroy(&attr);

	/// stampa conclusiva risultato SOMMA
	char buffer[200];
	sprintf(buffer, "\nRisultato finale della somma pari a: %d\n", *SUM);
	_println(buffer);

	clean_exit(0);
}
