/**
 *	@file gp_utils.c contiene alcune funzioni general-purpose, utili per il progetto.
*	@brief Funzioni general purpose
 **/
#ifndef GP_C
#define GP_C

/**
 * @brief La funzione _malloc usa la funzione di libreria malloc ma se questa dà errore chiude il programma
 * @param bytes: quantità di bytes da allocare
 * @return void *: puntatore alla memoria allocata
 **/
void *_malloc(int bytes){
	void *pointer_result;
	if((pointer_result = malloc(bytes)) == NULL)
		critical_err("Malloc");
	return pointer_result;
}

/**
 * @brief La funzione valid_chars conta i caratteri validi di una stringa
 * @param *buffer: Stringa da leggere
 * @return int: # caratteri validi
 **/
int valid_chars(char *buffer) {
	int counter = 0;
	while (*buffer++ != '\0') { /// Conto finchè non trovo \0
		counter++;
	}
	// considero l'ultimo carattere
	return counter;
}

#endif
