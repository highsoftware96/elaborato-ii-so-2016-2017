/**
 * 	@file io_utils.c: Questo file contiene funzioni essenziali per la lettura e scrittura dei file che vengono inseriti in input da linea di comando. I primi due file sono letti e memorizzati per comporre le matrici A e B, l'ultimo file invece viene scritto e contiene la matrice C, prodotto di A e B.
 *	@brief Funzioni di IO
 */

#ifndef IO_C
#define IO_C

/**
 * @brief La funzione read_file legge un file contenente una matrice e lo inserisce, intero per intero, nella memoria. Prima leggo un char alla volta, se incontro uno spazio o un \n allora ho completato un numero e converto il buffer
 * @param *path: puntatore alla stringa contenente il path del file da leggere
 * @param matrix: puntatore della memoria alla matrice su cui scrivere i dati
 * @return void
 **/
void read_file(char *path, int **matrix) {

	int file_descriptor = open(path, O_RDONLY, 0444); /// apertura del file in sola lettura

	if (file_descriptor == -1) { /// situazione di errore
		critical_err("open");
		clean_exit(1);
	}

	// lettura del file
	char temp_buffer; /// variabile per contenere il char letto
	int row = 0, col = 0;

	// max numero: 20 cifre!!
	char current_int_string[21] = { }; /// stringa per contenere tutte le cifre del numero
	int current_int_string_index = 0; /// indice per la stringa

	while (read(file_descriptor, &temp_buffer, sizeof(char)) > 0) { /// finchè la read di un char non è in fondo al file

		if (temp_buffer == ' ') { /// finito un numero

			signed int temp_number = strtol(current_int_string, NULL, 10); /// converto la stringa letta in un numero
			matrix[row][col] = temp_number; /// salvo il numero nella memoria condivisa

			// svuoto l'array dei caratteri
			// memset copia un carattere per i primi size_t caratteri della stringa
			memset(current_int_string, '\0', sizeof(current_int_string)); /// svuoto la stringa
			current_int_string_index = 0;
			col++;
		} else if (temp_buffer == '\n') { /// finita la riga
			signed int temp_number = strtol(current_int_string, NULL, 10); /// salvo il numero
			matrix[row][col] = temp_number; /// scrivo in memoria condivisa

			memset(current_int_string, '\0', sizeof(current_int_string)); /// svuoto la stringa
			current_int_string_index = 0;
			col = 0;
			row++;
		} else if ((temp_buffer >= '0' && temp_buffer <= '9') || temp_buffer == '-' ) {
			// mi salvo il carattere del numero corrente
			current_int_string[current_int_string_index++] = temp_buffer; /// salvo nell'array e incremento l'indice
		}
	}

	/// ultimo carattere
	if (current_int_string[0] != '\0') { // non siamo su una riga vuota
		int temp_number = strtol(current_int_string, NULL, 10);
		matrix[row][col] = temp_number;
	}

	close(file_descriptor); /// chiusura del file di input
}

/**
 * @brief La funzione write_result_matrix prende in input la memoria in cui c'è la matrice risultante C e la stampa su file intero per intero.
 *	@param file_descriptor: Intero descrittore del file di output su cui scrivere la matrice C
 *	@param matrix_C: Puntatore alla memoria in cui è stata salvata la matrice C risultante.
 **/
void write_result_matrix(int file_descriptor, int **matrix_C) {

	int i, row = 0, col = 0;

	for (i = 0; i < (size_global * size_global); i++) { /// per tutte le celle di C
		char buffer[21]; /// buffer per il numero
		sprintf(buffer, "%d", matrix_C[row][col]);

		write(file_descriptor, buffer, valid_chars(buffer)); /// scrittura sul descrittore di output

		if ((i + 1) % size_global == 0) /// sono a fine riga: \n
			write(file_descriptor, "\n", sizeof(char));
		else { /// finito il numero
			write(file_descriptor, " ", sizeof(char)); /// spazio
		}

		col++;
		if (col == size_global) {
			col = 0;
			row++;
		}
	}
}

/**
 *@brief Funzione per la scrittura tramite System Call a video (con caporiga finale)
 *@param buffer: stringa da stampare
 */
void _println(char *buffer) {
	char total_buffer[200];
	strcpy(total_buffer, buffer);
	strcat(total_buffer, "\n");
	write(1, total_buffer, valid_chars(total_buffer) + 1);
}

/**
 *@brief Funzione per la scrittura tramite System Call a video
 *@param buffer: stringa da stampare
 */
void _printf(char *buffer) {
	write(1, buffer, valid_chars(buffer) + 1);
}

#endif
