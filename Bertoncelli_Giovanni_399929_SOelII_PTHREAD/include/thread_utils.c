/*
 * thread_utils.c
 *
 *  Created on: 23 giu 2017
 *      Author: gberto96
 */
/**
 *@file thread_utils.c contiene le funzioni che saranno eseguite esclusivamente dalle thread
 */
/**
 @brief Funzione che moltiplica una riga per una colonna e
 posiziona il risultato nella matrice risultante
 @param row: riga della cella di c da moltiplicare
 @param column: colonna
 */
void multiply(int row, int column) {

	/**
	 eseguire l'operazione riga * colonna
	 e memorizzare il risultato in MC
	 */
	MC[row][column] = 0;

	for (int j = 0; j < size_global; j++) {
		MC[row][column] += (MA[row][j] * MB[j][column]);
	}
}

/**
 @brief Funzione di partenza delle thread per la moltiplicazione
 @param arg: package_t * della struttura utile alla thread
 */
void *thread_multiply_routine(void *arg) {
	/// controllo parametri passati

	/** l'argomento della funzione deve essere castato al tipo desiderato */
	package_t *thread_data = (package_t *) arg;

	char buffer[200];
	sprintf(buffer, "(MOLTIPLICAZIONE) Thread n. %d, indici: %d, %d\n",
			thread_data->id, thread_data->Arow, thread_data->Bcol);
	_printf(buffer);

	/** chiamata alla funzione che esegue il prodotto riga * colonna */
	multiply(thread_data->Arow, thread_data->Bcol);

	/** liberare lo spazio allocato per l'argomento della funzione */
	free(thread_data);

	return (NULL);

}

/**
 @brief Funzione che somma una riga della matrice C
 @param row: riga della cella di c da sommare
 @param mutex_sum: semaforo per la sincronizzazione tra le thread
 */
void sum(int row, pthread_mutex_t *mutex_sum) {

	///P(mutex)
	if (pthread_mutex_lock(mutex_sum) == -1) {
		critical_err("Errore lock mutex!");
	}

	/// somma della riga
	for (int j = 0; j < size_global; j++) {
		*SUM += MC[row][j];
	}

	///V(mutex)
	if (pthread_mutex_unlock(mutex_sum) == -1) {
		critical_err("Errore unlock mutex!");
	}

}

/**
 @brief Funzione di partenza delle thread per la somma
 @param arg: package_t_sum * della struttura utile alla thread
 */
void *thread_sum_routine(void *arg) {
	/// controllo parametri passati

	/** l'argomento della funzione deve essere castato al tipo desiderato */
	package_t_sum *thread_data = (package_t_sum *) arg;

	/** chiamata alla funzione che esegue la somma */
	sum(thread_data->Crow, thread_data->mutex);

	char buffer[200];
	sprintf(buffer, "(SOMMA) Thread, riga numero: %d\n", thread_data->Crow);
	_printf(buffer);

	/** liberare lo spazio allocato per l'argomento della funzione */
	free(thread_data);

	return NULL;

}
